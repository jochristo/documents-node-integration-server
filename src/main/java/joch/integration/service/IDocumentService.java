package joch.integration.service;

import joch.integration.model.DocumentModel;
import joch.integration.model.DocumentUploadParameter;
import joch.integration.model.FileDocument;

/**
 *
 * @author Admin
 */
public interface IDocumentService {
    
    public void createDocument(DocumentUploadParameter parameter);
    
    public DocumentModel create(DocumentUploadParameter parameter);
    
    public FileDocument getDocument(String id);    
    
    public FileDocument getDocumentAsync(String id);       
    
}
