/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joch.integration.service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import joch.integration.gateway.IDocumentGateway;
import joch.integration.model.DocumentModel;
import joch.integration.model.DocumentUploadParameter;
import joch.integration.model.FileDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Admin
 */
@Service
public class DocumentServiceImpl implements IDocumentService
{
    private static final Logger logger = LoggerFactory.getLogger(DocumentServiceImpl.class);
    
    @Autowired
    private IDocumentGateway gateway;
    
    @Override
    public DocumentModel create(DocumentUploadParameter parameter) {
        try {
            Future<DocumentModel> future = gateway.uploadAsync(parameter);
            return future.get(5, TimeUnit.SECONDS);                        
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            logger.error(ex.getMessage(), ex);
        } 
        return null;
    }
    

    @Override
    public FileDocument getDocument(String id) {
        FileDocument future = gateway.getDocument(id);
        return future;
    }
    
    @Override
    public FileDocument getDocumentAsync(String id)
    {        
        try {
            Future<FileDocument> future  = gateway.getDocumentAsync(id);
            return future.get(2, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }    

    @Override
    public void createDocument(DocumentUploadParameter parameter) {
        gateway.uploadAsync(parameter);
    }


    
    
    
}
