package joch.integration.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Admin
 */
@Configuration
@EnableRabbit
public class RabbitMqConfiguration
{
    public static final String DOCUMENT_REQUEST_EXCHANGE = "documentRequestsExchange";    
    public static final String DOCUMENT_RESPONSE_EXCHANGE = "documentResponseExchange";
    public static final String DOCUMENT_REQUEST_QUEUE = "documentRequestQueue";      
    public static final String DOCUMENT_RESPONSE_QUEUE = "documentResponseQueue";       
    
    public static final String DOCUMENTS_NODE_EXCHANGE_NAME = "app.documents.node";
    public static final String DOCUMENTS_NODE_REQUEST_QUEUE_NAME = "app.documents.request";
    public static final String DOCUMENTS_NODE_REPLY_QUEUE_NAME = "app.documents.reply";
    public static final String DOCUMENTS_NODE_ROUTING_KEY_NAME = "docu";    
    
    @Autowired private ConnectionFactory connectionFactory;    
    @Autowired private RabbitTemplate rabbitTemplate;        
    @Autowired private ApplicationContext context;    
    
    @Bean
    public Queue requestQueue() {
        return QueueBuilder.durable(DOCUMENTS_NODE_REQUEST_QUEUE_NAME).build();
    }      
    
    @Bean
    public Queue replyQueue() {
        return QueueBuilder.durable(DOCUMENTS_NODE_REPLY_QUEUE_NAME).build();
    }      
    
    @Bean
    public DirectExchange exchange() {
        return new DirectExchange(DOCUMENTS_NODE_EXCHANGE_NAME);
    }      
 
    @Bean
    public DirectExchange documentRequestsExchange() {
        //return ExchangeBuilder.topicExchange(DOCUMENT_REQUEST_EXCHANGE).build();
        return new DirectExchange(DOCUMENTS_NODE_EXCHANGE_NAME);
    }
 
    @Bean
    public DirectExchange documentResponseExchange() {
        //return ExchangeBuilder.topicExchange(DOCUMENT_RESPONSE_EXCHANGE).build();
        return new DirectExchange(DOCUMENTS_NODE_EXCHANGE_NAME);
    }    
    
    /*
    @Bean
    public Binding bindingOne(Queue documentRequestQueue, TopicExchange documentRequestsExchange) {
        return BindingBuilder.bind(documentRequestQueue).to(documentRequestsExchange).with(DOCUMENT_REQUEST_QUEUE);
    } 
    
    @Bean
    public Binding bindingTwo(Queue documentResponseQueue, TopicExchange documentResponseExchange) {
        return BindingBuilder.bind(documentResponseQueue).to(documentResponseExchange).with(DOCUMENT_RESPONSE_QUEUE);
    }  
    */
    
    //@Bean
    public Binding requestBinding() {
        return BindingBuilder.bind(requestQueue()).to(exchange()).with(DOCUMENTS_NODE_ROUTING_KEY_NAME);
    }

    @Bean
    public Binding replyBinding() {
        return BindingBuilder.bind(replyQueue()).to(exchange()).with(DOCUMENTS_NODE_ROUTING_KEY_NAME);
    }      
    
    
    //@Bean
    public AsyncRabbitTemplate asyncRabbitTemplate() {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        //Queue requestQueue = (Queue) context.getBean(DOCUMENT_REQUEST_QUEUE);
        //container.setQueues(requestQueue);        
        //return new AsyncRabbitTemplate(rabbitTemplate, container, DOCUMENT_RESPONSE_EXCHANGE);
        //container.setQueues(requestQueue());    
        container.setQueues(requestQueue());
        return new AsyncRabbitTemplate(rabbitTemplate, container);        
    } 
        
}
