package joch.integration.http;


/**
 *
 * @author ic
 */
public abstract class AbstractRestTemplateException extends RuntimeException
{
    public AbstractRestTemplateException(String statusCode, String message) {
        super( message);
    }
  
}
