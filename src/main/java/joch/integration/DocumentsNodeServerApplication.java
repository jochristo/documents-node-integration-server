package joch.integration;

import java.io.File;
import joch.integration.handler.DocumentRequestHandler;
import joch.integration.model.DocumentUploadParameter;
import joch.integration.service.IDocumentService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.SerializationUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 *
 * @author Admin
 */
@SpringBootApplication
@EnableRabbit
public class DocumentsNodeServerApplication //implements CommandLineRunner
{                  
    private static final Logger logger = LoggerFactory.getLogger(DocumentRequestHandler.class);
    private final IDocumentService service;    
    //private final RabbitTemplate rabbitTemplate;        
    //private final AsyncRabbitTemplate asyncRabbitTemplate;
    
    @Autowired
    public DocumentsNodeServerApplication(IDocumentService service)// , AsyncRabbitTemplate asyncRabbitTemplate) {
    {
        this.service = service;
        //this.rabbitTemplate = rabbitTemplate;
        //this.asyncRabbitTemplate = asyncRabbitTemplate;
    }
            
    
    public static void main(String[] args) {
        //ApplicationContext context = new AnnotationConfigApplicationContext(IntegrationConfiguration.class);
        SpringApplication.run(DocumentsNodeServerApplication.class, args);                  
    } 

    //@Override
    public void run(String... args) throws Exception
    {        
        System.out.println("documentService: " + service );
        
        for(int i = 0; i<5; i++){
            DocumentUploadParameter parameter = new DocumentUploadParameter();
            parameter.setIntegrity("integrity hash");
            File file;
            file = new File("C:\\TEMP\\integration.txt");               
            DiskFileItem fileItem = new DiskFileItem("file", "text/plain", false, file.getName(), (int) file.length(), file.getParentFile());
            fileItem.getOutputStream();
            MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
            parameter.setFile(multipartFile);        
            parameter.setIntegrity(DigestUtils.sha256Hex(fileItem.get()));
                    
            byte[] data = SerializationUtils.serialize(parameter);
            //byte[] data = parameter.getIntegrity().getBytes();
            //MessageProperties properties = MessagePropertiesBuilder.newInstance().setCorrelationId(message.getHeaders().get("amqp_correlationId").toString()).build();
        
            
            //asyncRabbitTemplate.convertSendAndReceive(RabbitMqConfiguration.DOCUMENT_REQUEST_QUEUE, MessageBuilder.withBody(data).build());
            //DocumentModel dm = service.create(parameter);             
        }
    }        

  
}
