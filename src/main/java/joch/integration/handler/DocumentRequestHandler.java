package joch.integration.handler;

import java.net.URI;
import joch.integration.http.BaseRestTemplateEngine;
import joch.integration.model.DocumentUploadParameter;
import joch.integration.model.FileDocument;
import joch.integration.service.IDocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Admin
 */
public class DocumentRequestHandler implements MessageHandler
{
    private static final Logger logger = LoggerFactory.getLogger(DocumentRequestHandler.class);
    
    @Autowired
    private BaseRestTemplateEngine restTemplate;
    
    @Autowired
    private IDocumentService documentService;
    
    @Override
    public void handleMessage(Message<?> message) throws MessagingException {
        
        try {
            MessageChannel replyChannel = (MessageChannel) message.getHeaders().get("replyChannel");
            logger.info("Requesting Document upload info, replyChannel channel: " + replyChannel.toString());
            //DocumentUploadParameter payload =  (DocumentUploadParameter) message.getPayload();
            String payload =  (String) message.getPayload();
            logger.info("Message payload: " + payload.toString());
            logger.info("Querying document repository service...");
            RestTemplate rest = new RestTemplate();
            
            //DocumentModel document = rest.postForObject("http://localhost:13333/rest/v1/documents", payload, DocumentModel.class);
            //HttpHeaders headers = new HttpHeaders();
            //headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            //HttpEntity<DocumentUploadParameter> request = new HttpEntity<>(payload);
            //ResponseEntity<DocumentModel> document  = rest.exchange("http://localhost:13333/rest/v1/documents", HttpMethod.POST, request, DocumentModel.class);
            //DocumentModel document = restTemplate.post("http://localhost:13333/rest/v1/documents", payload, DocumentModel.class);                       
            //DocumentModel document = rest.postForObject("http://localhost:13333/rest/v1/documents", requestEntity(payload), DocumentModel.class);            
            FileDocument document = rest.getForObject(new URI("http://localhost:33333/rest/v1/documents/" + payload), FileDocument.class);            
            logger.info("Received Document model from rest service: " + document.getId());
            replyChannel.send(MessageBuilder.withPayload(document).build());                    
            logger.info("Forwarding FileDocument message to" + replyChannel.toString());            
            
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        
    }
    
    private static HttpEntity<MultiValueMap<String, Object>> requestEntity (DocumentUploadParameter payload)
    {
        MultiValueMap<String, Object> multipartRequest = new LinkedMultiValueMap<>();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.MULTIPART_FORM_DATA);
        
        // creating an HttpEntity for the string part
        HttpHeaders stringHeader = new HttpHeaders();
        stringHeader.setContentType(MediaType.TEXT_PLAIN);
        HttpEntity<String> integrityEntity = new HttpEntity<>(payload.getIntegrity(), stringHeader);

        // creating an HttpEntity for the binary part
        HttpHeaders pictureHeader = new HttpHeaders();
        pictureHeader.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        HttpEntity<MultipartFile> fileEntity = new HttpEntity<>(payload.getFile(), pictureHeader);

        // putting the two parts in one request
        multipartRequest.add("integrity", integrityEntity);
        multipartRequest.add("file", fileEntity);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(multipartRequest, header);        
        return requestEntity;
    }
    
}
