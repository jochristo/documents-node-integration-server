package joch.integration.handler;

import joch.integration.configuration.RabbitMqConfiguration;
import joch.integration.model.DocumentModel;
import joch.integration.model.DocumentUploadParameter;
import joch.integration.service.IDocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

/**
 *
 * @author Admin
 */
@Service
public class RabbitMQHandler
{   
    private static final Logger logger = LoggerFactory.getLogger(RabbitMQHandler.class);
    
    @Autowired private RabbitTemplate rabbitTemplate;        
    //@Autowired private AsyncRabbitTemplate asyncRabbitTemplate;
    
    private int counter = 0;
    //@Autowired
    //private IDocumentGateway gateway;
    
    @Autowired IDocumentService service;
    
    @RabbitListener(queues = RabbitMqConfiguration.DOCUMENTS_NODE_REQUEST_QUEUE_NAME)
    public void handleDocumentRequestMessage(Message<?> message)
    {
        try
        {                        
            // read message
            DocumentUploadParameter parameter = (DocumentUploadParameter) SerializationUtils.deserialize((byte[]) message.getPayload());
            //logger.info("Rabbit received message#" +counter +" : " + parameter.toString());
            logger.info("Rabbit received message with headers: " + message.getHeaders());
            
            counter++;
            
            // contact external document-node repository API            
            DocumentModel dm = service.create(parameter);            

            byte[] data = SerializationUtils.serialize(dm);
            //MessageProperties properties = MessagePropertiesBuilder.newInstance().setCorrelationId(message.getHeaders().get("amqp_correlationId").toString()).build();
            //logger.info("Rabbit is sending message to exchange " + RabbitMqConfiguration.DOCUMENT_RESPONSE_EXCHANGE + " and queue " + RabbitMqConfiguration.DOCUMENT_RESPONSE_QUEUE);
            //rabbitTemplate.convertAndSend( RabbitMqConfiguration.DOCUMENT_REQUEST_EXCHANGE, RabbitMqConfiguration.DOCUMENT_RESPONSE_QUEUE, MessageBuilder.withBody(data).build());            
            
            logger.info("Rabbit is sending message with routing key " + RabbitMqConfiguration.DOCUMENTS_NODE_REPLY_QUEUE_NAME + ", body: " + dm.toString());
            rabbitTemplate.convertAndSend( RabbitMqConfiguration.DOCUMENTS_NODE_EXCHANGE_NAME, RabbitMqConfiguration.DOCUMENTS_NODE_REPLY_QUEUE_NAME, MessageBuilder.withBody(data).build());            
            //asyncRabbitTemplate.convertSendAndReceive(RabbitMqConfiguration.DOCUMENTS_NODE_EXCHANGE_NAME, RabbitMqConfiguration.DOCUMENTS_NODE_ROUTING_KEY_NAME, MessageBuilder.withBody(data).build());            
        }
        catch (AmqpException ex)
        {
            logger.error(ex.getMessage(), ex);
        }
    }
    
    /*
    @RabbitListener(queues = RabbitMqConfiguration.DOCUMENT_RESPONSE_QUEUE)
    public void handleDocumentResponseMessage(Message<?> message) throws MessagingException, InterruptedException, ExecutionException
    {
        logger.info("Rabbit listened message: " + message.getPayload().toString());
        
        // read message
        DocumentModel documentModel = (DocumentModel) SerializationUtils.deserialize((byte[]) message.getPayload());
        logger.info("Document info received from Mongo: " + documentModel.toString());                        
    }    
    */
}
