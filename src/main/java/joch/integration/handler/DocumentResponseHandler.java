package joch.integration.handler;

import joch.integration.model.DocumentModel;
import joch.integration.model.FileDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

/**
 *
 * @author Admin
 */
public class DocumentResponseHandler implements MessageHandler
{
    private static final Logger logger = LoggerFactory.getLogger(DocumentResponseHandler.class);
    
    //@Override
    public void handleMessage1(Message<?> message) throws MessagingException
    {
	MessageChannel inputChannel = (MessageChannel) message.getHeaders().get("replyChannel");
	logger.info("Requesting Document model info, Input Channel: " + inputChannel.toString());                
        DocumentModel payload =  (DocumentModel) message.getPayload();
        logger.info("Message payload: " + payload.toString());          
    }
    
    @Override
    public void handleMessage(Message<?> message) throws MessagingException
    {
	MessageChannel inputChannel = (MessageChannel) message.getHeaders().get("replyChannel");
	logger.info("Requesting FileDocument info, Input Channel: " + inputChannel.toString());                
        FileDocument payload =  (FileDocument) message.getPayload();
        logger.info("Message payload: " + payload.toString());          

    }    
    
}
