package joch.integration;

import java.io.File;
import java.io.IOException;
import joch.integration.gateway.IDocumentGateway;
import joch.integration.handler.DocumentRequestHandler;
import joch.integration.handler.DocumentResponseHandler;
import joch.integration.model.DocumentUploadParameter;
import joch.integration.service.DocumentServiceImpl;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 *
 * @author Admin
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class TestIntegrationGateway {

    @Autowired private IDocumentGateway gateway;    
    @Autowired private DocumentServiceImpl documentService;

    @Autowired
    @Qualifier("documentRequestChannel")
    private ExecutorChannel requestChannel;

    @Autowired
    @Qualifier("documentReplyChannel")
    private ExecutorChannel replyChannel;

    //@Before
    public void testDependencies() {
        Assert.assertNotNull(gateway);
    }

    //@Test
    public void testSimpleMessageReceiving() throws IOException {

        DocumentUploadParameter parameter = new DocumentUploadParameter();
        parameter.setIntegrity("integrity hash");
        File file;
        file = new File("C:\\TEMP\\integration.txt");
        
        DiskFileItem fileItem = new DiskFileItem("file", "text/plain", false, file.getName(), (int) file.length(), file.getParentFile());
        fileItem.getOutputStream();
        MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
        parameter.setFile(multipartFile);

        requestChannel.subscribe(new DocumentRequestHandler());                                
        replyChannel.subscribe(new DocumentResponseHandler());
        documentService.getDocument("5ae95e1a25c0513528e28040");
        
        System.out.println("Reply channel send count: " + replyChannel.getSendCount());
        System.out.println("Reply channel last date message sent: " +replyChannel.getTimeSinceLastSend());
        //documentService.getDocument("5ae95e1a25c0513528e28040");
        //requestChannel.unsubscribe(new DocumentRequestHandler());
        //replyChannel.unsubscribe(new DocumentResponseHandler());           
        //requestChannel.subscribe(handler)
        //assertEquals("Object sent through Gateway should be milk", msg.getPayload(), milk);
    }

}
